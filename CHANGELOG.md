# [1.1.0](https://gitlab.com/rk-ci-components/semantic-release/compare/1.0.2...1.1.0) (2024-04-06)


### Features

* implemented core component ([b80b73f](https://gitlab.com/rk-ci-components/semantic-release/commit/b80b73fa6a80be2f7184c09223a40078371e284e))

## [1.0.2](https://gitlab.com/rk-ci-components/semantic-release/compare/1.0.1...1.0.2) (2024-04-01)


### Bug Fixes

* **changelog:** fix changelog creation ([82927e7](https://gitlab.com/rk-ci-components/semantic-release/commit/82927e765752012d26a17f30712fa63c566baf03))

## [1.0.1](https://gitlab.com/rk-ci-components/semantic-release/compare/v1.0.0...1.0.1) (2024-04-01)


### Bug Fixes

* **changelog:** fix changelog creation ([49bdf28](https://gitlab.com/rk-ci-components/semantic-release/commit/49bdf28ebb0d56da15609d5e083766fd3b49eb87))

# 1.0.0 (2024-04-01)


### Features

* **initial:** initial release ([2c1f2cd](https://gitlab.com/rk-ci-components/semantic-release/commit/2c1f2cd66b30fe0316e5fc9a68d782a8d076f284))
