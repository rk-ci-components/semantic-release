# GitLab CI component for semantic-release

[![Latest Release](https://gitlab.com/rk-ci-components/semantic-release/-/badges/release.svg)](https://gitlab.com/rk-ci-components/semantic-release/-/releases)
[![pipeline status](https://gitlab.com/rk-ci-components/semantic-release/badges/master/pipeline.svg)](https://gitlab.com/rk-ci-components/semantic-release/-/commits/master)

This project implements a GitLab CI/CD template to automate your versioning and release management with [semantic-release](https://github.com/semantic-release/semantic-release), supporting one or several of the following features:

* determine the next release version number;
* generate and commit the changelog;
* create and push the Git tag;

## Usage

This template can be used both as a [CI/CD component](https://docs.gitlab.com/ee/ci/components/#use-a-component-in-a-cicd-configuration).

### Use as a CI/CD component

Add the following to your `gitlab-ci.yml`:

```yaml
include:
  - component: gitlab.com/rk-ci-components/semantic-release/semantic-release
    inputs:
      auto-release: true
```

## Global configuration

The semantic-release template uses some global configuration used throughout all jobs.

| Input / Variable                                         | Description                                                                                                                                                                                                                                                                                                                                                                                                                                                                                          | Default value                 |
|----------------------------------------------------------|------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|-------------------------------|
| :lock: `GITLAB_TOKEN`                                    | A GitLab [project access token](https://docs.gitlab.com/ee/user/project/settings/project_access_tokens.html) or [personal access token](https://docs.gitlab.com/ce/user/profile/personal_access_tokens.html) with `api`, `read_repository` and `write repository` scopes. :warning: This variable is **mandatory** and [defined by `semantic-release`](https://github.com/semantic-release/semantic-release/blob/master/docs/usage/ci-configuration.md#push-access-to-the-remote-repository) itself. | _none_                        |
| `auto-release` / `AUTO_RELEASE`                          | Defines if we want to auto release on pushes to the production branch (master/main).                                                                                                                                                                                                                                                                                                                                                                                                                 | `false`                       |
